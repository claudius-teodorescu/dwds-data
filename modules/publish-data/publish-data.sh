# env variables
CI_PROJECT_DIR=${PWD}
if [ -z ${CI_PAGES_URL+x} ]; then CI_PAGES_URL="https://claudius-teodorescu.gitlab.io/dwds-data/"; else echo "var is set to '$var'"; fi

text_dir_path=$CI_PROJECT_DIR/contents/text
peritext_dir_path=$CI_PROJECT_DIR/contents/peritext/indexes
public_dir_path=$CI_PROJECT_DIR/public
published_indexes_dir_path=$public_dir_path/peritext/indexes
aggregated_index_dir_path=$published_indexes_dir_path/_aggregated

rm -rf $public_dir_path
mkdir -p {$published_indexes_dir_path,$aggregated_index_dir_path}

echo "loop over the index definitions, in order to generate each index and metadata for it"
time find $peritext_dir_path -type f -name "metadata.ttl" | while read file_path; do
    index_abbreviation="$(dirname "$file_path")"
    index_abbreviation="$(basename "$index_abbreviation")"

    relative_path=$(echo $file_path | sed "s|$CI_PROJECT_DIR/contents/||")
    relative_path=$(echo "$relative_path" | sed -e 's/\(.*\)/\L\1/')

    current_published_index_dir_path=$published_indexes_dir_path/$index_abbreviation

    echo "csv to index as triples for $index_abbreviation"
    time $CI_PROJECT_DIR/modules/csv-to-automatic-index/csv-to-automatic-index $peritext_dir_path/$index_abbreviation/metadata.ttl $text_dir_path "**/*.csv" $published_indexes_dir_path $index_abbreviation

    echo "index as triples to metadata for $index_abbreviation"
    time $CI_PROJECT_DIR/modules/index-as-triples-to-metadata/index-as-triples-to-metadata $current_published_index_dir_path/index.ttl $peritext_dir_path/$index_abbreviation/metadata.ttl $CI_PAGES_URL/peritext/indexes/$index_abbreviation/metadata.ttl
done

echo "index-as-triples-to-compressed-index"
time $CI_PROJECT_DIR/modules/index-as-triples-to-compressed-index/index-as-triples-to-compressed-index -i $published_indexes_dir_path/**/index.ttl -a $aggregated_index_dir_path
